﻿function OnInit(initData) {
    initData.name = "Tab Group Title";
    initData.version = "1.0";
    // initData.copyright = "(c) 2023 Gqqnbig";
//	initData.url = "https://resource.dopus.com/c/buttons-scripts/16";
    initData.desc = "Set Lister (window) title to the group title";
    initData.default_enable = true;
    // 12.22 introduces Lister.tabgroupleft and tabgroupright properties.
    initData.min_version = "12.22";
}

function OnOpenTab(openTabData) {
    //DOpus.Output("OnTabClick");
    var lister = openTabData.tab.lister;

    var title = undefined;

    // DOpus.Output(lister.tabgroupleft);
    // DOpus.Output(lister.tabgroupright);
    if (lister.tabgroupleft)
        title = lister.tabgroupleft;
    if (lister.tabgroupright)
        title = title + " - " + lister.tabgroupright;

    if (title)
        runDopusCommand('Set LISTERTITLE "notoggle: 标签组 ' + title + '"', openTabData.tab);
}

function runDopusCommand(command, tab) {
    //debug("runDopusCommand '" + command + "'" + '" with tab ' + String(tab));

    var dopusCommand = DOpus.NewCommand;
    dopusCommand.SetSourceTab(tab);
    dopusCommand.RunCommand(command);
}

function debug(text) {
    DOpus.Output(text);
}
